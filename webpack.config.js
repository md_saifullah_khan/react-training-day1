const path = require("path");
module.exports = {
    entry:'./index.js',
    output:{
        path:path.resolve(__dirname,""),
        filename: 'bundle.js',
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            }
        ]
    }
}