import React from "react";
import {render} from "react-dom";

const App = ()=>(<div>Hello There</div>);

const target = document.getElementById("app");

render(<App/>,target);